''' Single page Flask app that takes a AMI data file and plots some graphs. '''
import sys
import io
import json
import numpy as np
import pandas as pd
from ami_data_parser import *
from ami_data_parser.columnize import *
from flask import Flask, session, redirect, url_for, escape, request, flash, render_template, \
  make_response
from flask import Flask, session
from flask_session import Session
from werkzeug.utils import secure_filename


app = Flask(__name__)
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
Session(app)
logger = app.logger

group_by_keys = ['mon', 'mday', 'wday', 'hour', 'qtr', 'ssn']
group_by_limit = 2
to_json_options = {
  'double_precision': 6,
  'date_unit': 's',
  'orient': 'index' # dict like {index -> {column -> value}}
}


@app.route('/', methods=['GET', 'POST'])
@app.route('/<string:key>', methods=['GET', 'POST'])
def upload(key='mon'):
  ''' Single page for initial data upload, and home for post upload statuses etc. '''
  data = meta = splits = payload = None
  if request.method == 'POST':
    if 'file' in request.files:
      try:
        (data, meta) = _set_data(request.files['file'])
      except AmiDataParserException:
        flash(e, 'error')
  if 'data' in session:
    logger.debug('GET and session')
    if key in group_by_keys:
      (data, meta) = _get_data()
      data = data.loc[:,['value'] + [key]]    
      data = data.groupby(key).agg(('count', 'mean', 'min', 'max', 'std'))
      data.columns = data.columns.levels[1] # Fix Panda's uselessly using a Multiindex.
      payload = json.loads(data.to_json(**to_json_options))
    else:
      flash('Invalid argument', 'error')
  else:
    flash('No data uploaded', 'error')
  return render_template('main.html', payload=payload, meta=meta, splits=group_by_keys)
    

def _get_data():
  if 'data' not in session:
    return (None, None)
  data = pd.read_csv(io.StringIO(session['data']))
  return (data, session['meta'])


def _set_data(f):
  (data, meta) = _parse_uploaded_data_file(f)
  session['data'] = data.to_csv(index=False)
  meta.update({'filename': secure_filename(f.filename)})
  session['meta'] = meta
  logger.debug('Saved session data [%s - %s] [%d]', meta['start'], meta['end'], len(data))
  return (data, meta)


def _parse_uploaded_data_file(f):
  (df, meta) = parse(f)
  return (columnize(df), meta)
